package test;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import graph.undirected.UndirectedGraph;

import java.util.*;

public class UndirectedGraphTest {

	@Test
    public void testNullGraph(){
		UndirectedGraph<String> g1 = null;
		assertEquals(g1, null);
	}
	
	@Test 
    public void testEmptyGraph(){
		UndirectedGraph<String> g2 = new UndirectedGraph<String>();
		assertTrue(g2 != null);	
	}
	
	@Test 
    public void testAddNode(){
		UndirectedGraph<String> g3 = new UndirectedGraph<String>();
		g3.addNode("flirt");
		assertTrue(g3 != null);	
	}
	
	@Test 
    public void testAddEdge(){
		UndirectedGraph<String> g4 = new UndirectedGraph<String>();
		g4.addNode("fling");
		g4.addNode("cling");
		assertFalse(g4.hasEdge("fling", "cling"));
		g4.addEdge("fling", "cling");
		assertTrue(g4.hasEdge("fling", "cling"));
	}
	
	@Test(expected = NoSuchElementException.class)
	public void throwsExceptionOnNonexistentNode(){
		UndirectedGraph<String> g5 = new UndirectedGraph<String>();
		g5.hasEdge("fling", "cling");
	}
	
	
	@Test 
    public void testHasEdge(){
		UndirectedGraph<String> g6 = new UndirectedGraph<String>();
		g6.addNode("fling");
		g6.addNode("cling");
		assertFalse(g6.hasEdge("fling", "cling"));
		assertFalse(g6.hasEdge("cling", "fling"));
		g6.addEdge("fling", "cling");
		assertTrue(g6.hasEdge("fling", "cling"));
		assertTrue(g6.hasEdge("cling", "fling"));
	}
	
	@Test 
    public void testBfs(){
		UndirectedGraph<String> g7 = new UndirectedGraph<String>();
		g7.addNode("stone"); //v1
		g7.addNode("stony"); //v2
		g7.addNode("store"); //v3
		g7.addNode("atone"); //v4
		g7.addNode("alone"); //v5
		g7.addNode("clone"); //v6
		g7.addNode("crone"); //v7
		g7.addNode("drone"); //v8
		g7.addNode("crane"); //v9
		g7.addEdge("stone","store"); //e12
		g7.addEdge("stone","stony"); //e13
		g7.addEdge("stone","atone"); //e14
		g7.addEdge("atone","alone"); //e45
		g7.addEdge("alone","clone"); //e56
		g7.addEdge("clone","crone"); //e67
		g7.addEdge("crone","drone"); //e78
		g7.addEdge("crone","crane"); //e79
		assertEquals(g7.bfs("atone"),new LinkedList<String>(Arrays.asList("atone","alone","stone","clone","stony","store","crone","crane","drone")));
	}
	
	
	@Test 
    public void testPathTo(){
		UndirectedGraph<String> g8 = new UndirectedGraph<String>();
		g8.addNode("stone"); //v1
		g8.addNode("stony"); //v2
		g8.addNode("store"); //v3
		g8.addNode("atone"); //v4
		g8.addNode("alone"); //v5
		g8.addEdge("stone","store"); //e12
		g8.addEdge("stone","stony"); //e13
		g8.addEdge("stone","atone"); //e14
		g8.bfs("stone");
		assertEquals(g8.pathTo("stone"),new LinkedList<String>(Arrays.asList("stone"))); 
		assertEquals(g8.pathTo("atone"),new LinkedList<String>(Arrays.asList("stone","atone")));
		assertNotEquals(g8.pathTo("atone"),new LinkedList<String>(Arrays.asList("stony","store"))); 
		assertEquals(g8.pathTo("alone"),null);
	}
	
	@Test 
    public void testHasPathTo(){
		UndirectedGraph<String> g9 = new UndirectedGraph<String>();
		g9.addNode("stone"); //v1
		g9.addNode("stony"); //v2
		g9.addNode("store"); //v3
		g9.addNode("atone"); //v4
		g9.addNode("alone"); //v5
		g9.addNode("clone"); //v6
		g9.addEdge("stone","store"); //e12
		g9.addEdge("stone","stony"); //e13
		g9.addEdge("stone","atone"); //e14
		g9.bfs("stony");
		assertTrue(g9.hasPathTo("stone")==true);
		assertTrue(g9.hasPathTo("clone")==false);
	}
	
	@Test 
    public void testDistTo(){
		UndirectedGraph<String> g10 = new UndirectedGraph<String>();
		g10.addNode("stone"); //v1
		g10.addNode("stony"); //v2
		g10.addNode("store"); //v3
		g10.addNode("atone"); //v4
		g10.addNode("alone"); //v5
		g10.addEdge("stone","store"); //e12
		g10.addEdge("stone","stony"); //e13
		g10.addEdge("stone","atone"); //e14
		g10.bfs("stone");
		assertTrue(g10.distTo("stone")==0); 
		assertFalse(g10.distTo("stone")==1);
		assertTrue(g10.distTo("stony")==1);
		assertTrue(g10.distTo("alone")==Integer.MAX_VALUE);
	}
	 
}