javac -d . Dictionary.java Graph.java DisconnectedException.java UndirectedGraph.java WordLadder.java main\Main.java
java main.Main words_1.txt dictionary.txt output_actual_1.txt
java main.Main words_2.txt dictionary.txt output_actual_2.txt
java main.Main words_3.txt dictionary.txt output_actual_3.txt
java main.Main words_4.txt dictionary.txt output_actual_4.txt
java main.Main words_5.txt dictionary.txt output_actual_5.txt
java main.Main words_6.txt dictionary.txt output_actual_6.txt
java main.Main words_7.txt dictionary.txt output_actual_7.txt

javac -cp junit-4.12.jar;hamcrest-core-1.3.jar -d . UndirectedGraphTest.java UndirectedGraph.java Graph.java DisconnectedException.java
java -cp .;junit-4.12.jar;hamcrest-core-1.3.jar org.junit.runner.JUnitCore test.UndirectedGraphTest

fc output_actual_1.txt output_expected_1.txt
fc output_actual_2.txt output_expected_2.txt
fc output_actual_3.txt output_expected_3.txt
fc output_actual_4.txt output_expected_4.txt
fc output_actual_5.txt output_expected_5.txt
fc output_actual_6.txt output_expected_6.txt
fc output_actual_7.txt output_expected_7.txt