package graph;

public interface Graph<V>
{
    boolean hasEdge(V v1, V v2);
    void addNode(V v);
    void addEdge(V v1, V v2);
}