package utils;
//https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/TreeSet.html
import java.util.TreeSet;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
//https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Objects.html
import java.util.Objects;

public class Dictionary
{
    private TreeSet<String> wordList = new TreeSet<>();
    public Dictionary(String fileName)
    {
        try (BufferedReader br = new BufferedReader(
            new FileReader(new File(fileName)))) {
            String line;
            while ((line = br.readLine()) != null) {
                wordList.add(line);
            }
        } catch (FileNotFoundException e) {
        } catch (IOException e) {}
    }
    public TreeSet<String> getWords()
    {
        //strings are immutable so just copy outer TreeSet
        return new TreeSet<String>(wordList);
    }
    @Override
    public boolean equals(Object o)
    {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof Dictionary)) return false;
        return wordList.equals(((Dictionary)o).wordList);
    }
    @Override
    public int hashCode()
    {
        //return Objects.hash(wordList);
        return wordList.hashCode();
    }
}
/*
//Wrong 1: Does not import BufferedReader, FileReader, File, IOException
//Wrong 2: Cannot make new Set<> interface, must make a new TreeSet
//Wrong 3: package is utils without class specified
//Wrong 4: Set requires add not put
//Wrong 5: .equals on null pointer would throw exception use: == null
//Wrong 6: Forgot to initialize reader to null (better is try-with-resources)
//Wrong 7: constructor on FileReader and File called without new operator
//Wrong 8: Does not explicitly handle file-not-found scenario
//Improvement 1: imports all of util, not just Set, TreeSet
//Improvement 2: could store Set<String> instead of TreeSet - more general

package utils.Dictionary;

import java.util.*;

public class Dictionary{

	protected TreeSet<String> wordList = new Set<String>();
	
	public Dictionary(String filePath) {
		BufferedReader reader;
        try {
            reader = new BufferedReader(FileReader(File(filePath)));
            for (String line = reader.readLine(); line.equals(null);
                line = reader.readLine()) {
				wordList.put(line);
			}
		} catch (IOException e) {
		} finally {
            if (reader != null) {
                reader.close();
            }
		}
	}
	
}*/