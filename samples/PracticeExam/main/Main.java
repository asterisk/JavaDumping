package main;

import ladder.WordLadder;
import utils.Dictionary;
import utils.DisconnectedException;

import java.util.List;
import java.io.*;

public class Main {

	public static void main(String[] args){
		if (args.length != 3) {
			System.err.println("Three arguments are required.");
			return;
		}

		String wordsFile = args[0]; // a file containing two words (the bottom and top of the layer)
		String dictionaryFile = args[1]; // the file containing the dictionary
		String outputFile = args[2]; // the output file
		
		try {
			WordLadder wL = new WordLadder();
			WordLadder.buildLadder(args[0],args[1],args[2]);
		} catch (DisconnectedException e) {
		} catch (IOException e) {} 
		
	}

}