package graph.undirected;

import graph.Graph;
import utils.DisconnectedException;
//https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/LinkedList.html
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.NoSuchElementException;

public class UndirectedGraph<V> implements Graph<V>
{
    private HashMap<V, HashSet<V>> neighborhoodList = new HashMap<>();
    private HashMap<V, V> bfsParents = new HashMap<>();
    public UndirectedGraph() {}
    public boolean hasEdge(V v1, V v2)
    {
        if (!neighborhoodList.containsKey(v1) ||
            !neighborhoodList.containsKey(v2))
            throw new NoSuchElementException("Nonexistent node.");
        if (neighborhoodList.get(v1).contains(v2) != 
            neighborhoodList.get(v2).contains(v1)) {
            throw new IllegalArgumentException();
        }
        return neighborhoodList.get(v1).contains(v2);
    }
    public void addNode(V v)
    {
        neighborhoodList.put(v, new HashSet<V>());
    }
    public void addEdge(V v1, V v2)
    {
        if (!neighborhoodList.containsKey(v1)) addNode(v1);
        if (!neighborhoodList.containsKey(v2)) addNode(v2);
        neighborhoodList.get(v1).add(v2);
        neighborhoodList.get(v2).add(v1);
    }
    public LinkedList<V> bfs(V start)
    {
        LinkedList<V> queue = new LinkedList<>();        
        HashSet<V> visited = new HashSet<>();
        LinkedList<V> order = new LinkedList<>();
        bfsParents.clear();
        queue.add(start);
        visited.add(start);
        bfsParents.put(start, null);
        while (!queue.isEmpty()) {
            V top = queue.remove();            
            order.add(top);
            for (V child : neighborhoodList.get(top)) {
                if (!visited.contains(child)) {
                    queue.add(child); visited.add(child);
                    bfsParents.put(child, top);
                }
            }
        }
        return order;
    }
    public LinkedList<V> pathTo(V end)
    {
        if (!hasPathTo(end)) return null;
        LinkedList<V> list = new LinkedList<>();
        do {
            list.addFirst(end);
        } while ((end = bfsParents.get(end)) != null);
        return list;
    }
    public int distTo(V end)
    {
        return hasPathTo(end) ? pathTo(end).size()-1 : Integer.MAX_VALUE;
    }
    public boolean hasPathTo(V end) //throws DisconnectedException
    {
        return bfsParents.containsKey(end);
        /*if (!bfsParents.containsKey(end)) {
            throw new DisconnectedException("Not connected!");
        }
        return true;*/
    }
}
