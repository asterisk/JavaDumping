package ladder;

import utils.Dictionary;
import utils.DisconnectedException;
import graph.undirected.UndirectedGraph;
import java.util.TreeSet;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class WordLadder
{
    public static void buildLadder(String fileName,
        String wordFile, String outputFile) throws IOException, DisconnectedException
    {
        Dictionary dict = new Dictionary(wordFile);
        UndirectedGraph<String> graph = new UndirectedGraph<>();
        TreeSet<String> words = dict.getWords();
        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                for (char c = 'a'; c <= 'z'; c++) {
                    if (c == word.charAt(i)) continue;
                    String str = word.substring(0, i) +
                        c + word.substring(i+1, word.length());
                    if (words.contains(str)) {
                        graph.addEdge(word, str);
                    }
                }
            }
        }
        try (BufferedReader br = new BufferedReader(
            new FileReader(new File(fileName)));
            PrintWriter pw = new PrintWriter(new File(outputFile))) {
            String line = br.readLine();
            if (line == null || br.readLine() != null) {
                pw.println("class java.lang.RuntimeException");
                pw.println("The input file does not consist of one line.");
                return;
            }
            String[] lines = line.split(",");
            if (lines.length != 2) {
                pw.println("class java.lang.RuntimeException");
                pw.println("The number of words in the input file in a line differs from two.");
                return;
            } else if (lines[0].length() != lines[1].length()) {
                pw.println("class java.lang.RuntimeException");
                pw.println("Words have different lengths.");
                return;
            }
            String startWord = lines[0], endWord = lines[1];        
            if (!words.contains(startWord)) {
                pw.println("class java.lang.RuntimeException");
                pw.println(startWord + " is not in the dictionary.");
                return;
            } else if (!words.contains(endWord)) {
                pw.println("class java.lang.RuntimeException");
                pw.println(endWord + " is not in the dictionary.");
                return;
            }
            graph.bfs(startWord);
            if (graph.hasPathTo(endWord)) {
                pw.println("The length of the path is " +
                    graph.distTo(endWord) + ".");
                pw.println(graph.pathTo(endWord));
            } else {
                pw.println("DISCONNECTED");
                throw new DisconnectedException();
            }
        }
    }
}