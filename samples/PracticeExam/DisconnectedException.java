package utils;

public class DisconnectedException extends Exception
{
    public DisconnectedException()
    {
        super("Not connected!");
    }
    public DisconnectedException(String message)
    {
        super(message);
    }
}