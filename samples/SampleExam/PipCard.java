package cards;

public class PipCard extends Card
{
    private int value;
    public PipCard(int value, Suit suit)
    {
        super(suit);
        this.value = value;
    }
    public String toString()
    {
        if (value == 1)
            return "ACE of " + super.toString();
        else return super.toString() + " (" + value + ")";
    }
}