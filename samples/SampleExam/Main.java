import cards.Deck;
import cards.Card;

public class Main
{
    public static void main(String[] args)
    {
        Deck deck = Deck.makeFrenchDeck();
        while (!deck.isEmpty()) {
            Card c = deck.draw();
            System.out.println(c);
        }
    }
}