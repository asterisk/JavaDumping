package cards;

public class FaceCard extends Card
{
    private Face face;
    public FaceCard(Face face, Suit suit)
    {
        super(suit);
        this.face = face;
    }
    public String toString() {
        return this.face.toString() + " of " + super.toString();
    }
}