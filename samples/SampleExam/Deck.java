package cards;

//https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/LinkedList.html
import java.util.LinkedList;

public class Deck
{
    private LinkedList<Card> cards = new LinkedList<>();
    public boolean isEmpty() { return cards.size() == 0; }
    public Card draw() {
        return cards.remove();
    }
    public static Deck makeFrenchDeck()
    {
        Deck deck = new Deck();
        for (Suit s : Suit.values()) {
            for (int i = 1; i <= 10; i++) {
                deck.cards.add(new PipCard(i, s));
            }
            for (Face f : Face.values()) {
                if (f == Face.CAVALIER) continue;
                deck.cards.add(new FaceCard(f, s));                
            }
        }
        return deck;
    }
}
