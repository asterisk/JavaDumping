class X{
    int a;
    X(int i) {a=i;}
}

class Y{
    int a;
    Y(int i ) {a=i;}
}

class Z extends X{
    int b;
    Z(int i, int j){
        super(j);
        b = i;
    }
}


class  hab {
    public static void main(String args[]){
        X x = new X(10);
        X x2;
        Y y = new Y(5);
        Z z;
        x2 = x;
        X x3;
        x3 = new Z(4,4);
        // z = new X(3); // should result in an error too
        // x2 = y; // should result in a compilations error
    }
}
