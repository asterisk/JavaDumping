class A{
    int i,j;
    A(int a, int b){
        i=a;
        j=b;
    }
    void show(){
        System.out.println( i +  j);
    }
}

class B extends A{
    int k;
    B(int a, int b, int c){
        super(a, b);
        k = c;
    }
    void show() {
        System.out.println(k);
    }
}

class Sup {
    void who () {
        System.out.println("who in sup");
    }
}

class Sub1 extends Sup{
    void who() {
        System.out.println("sub1");
    }
}

class Sub2 extends Sup{
    void who() {
        System.out.println("sub2");
    }
}

class Exp {
    void who(Sup i){
        i.who();
    }

    void IfWho(Sup i){
    }
}

class overriding{
    public static void main(String[] args){
        A a = new A(1,2);
        a.show();
        B b = new B(1,2,3);
        b.show();

        Sup SupRef;

        Sup SupObj= new Sup();
        Sub1 Sub1Obj = new Sub1();
        Sub2 Sub2Obj = new Sub2();

        SupRef = SupObj;
        SupRef.who();

        SupRef = Sub1Obj;
        SupRef.who();

        SupRef = Sub2Obj;
        SupRef.who();

        Exp ExpObj = new Exp();
        ExpObj.who(SupObj);
    }
}
