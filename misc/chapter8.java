class Outer{
    int nums[];
    Outer(int n[]){
        nums = n;
    }
    void analyze(){
        Inner ob = new Inner();
        System.out.println("Min: " + ob.min());
        System.out.println("Min: " + ob.max());
        System.out.println("Min: " + ob.avg());
    }

    class Inner{
        int min(){
            int m = nums[0];
            for(int i: nums)
                if(i< m) m = i;
            return m;
        }
        int max(){
            int m = nums[0];
            for(int i: nums)
                if(i> m) m = i;
            return m;
        }
        int avg(){
            int a = 0;
            for(int i: nums)
                a+=i;
            return a/nums.length;
        }
    }
}

class VarArgs{
    static void vaTest(int ... v){
    }
    static void vaTest(boolean... v){
    }
}


class chapter8{
    static public void main(String args[]){
        int x[] = {3,4,5,6,6,7,8};
        Outer ob = new Outer(x);
        ob.analyze();
        vaTest(10);
        vaTest(1,2,3);
        vaTest();
    }

    static void vaTest(int ... v){
        System.out.println("Vlength: " + v.length);
        System.out.println("cons: ");
        for(int i=0; i<v.length;i++)
            System.out.print(v[i] +"\n");
    }
}
