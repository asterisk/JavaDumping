class stat{
    int x;
    static int y;

    int sum(){
        return x+y;
    }
}

class Fac{
    int FactR(int n){
        if(n==0) return 1;
        if(n==1) return 1;
        return FactR(n-1) * n;
    }
}

class recursion{
    static public void main(String[] args){
        stat ob1 = new stat();
        stat ob2 = new stat();
        ob1.x = 3;
        ob2.x = 4;

        stat.y = 5;


        Fac ob = new Fac();
        System.out.println(ob.FactR(1));
    }
}
