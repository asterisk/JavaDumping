class Date{
    int y,m,d;
    Date(){
    }
    Date(int y, int m, int d){
        this.y=y;
        this.m=m;
        this.d=d;
    }
    public boolean equals(Object p){
        if (!(p instanceof Date)) return false;
        return y == ((Date)p).y &&
            m == ((Date)p).m &&
            d == ((Date)p).d;
    }
}

class misc{
    static public void main(String[] args){
        Date d1 = new Date(2001, 1, 1);
        Date d2 = new Date(2001, 1, 1);
        Date d3 = new Date(1789, 7, 14);
        char o = 'o';
        System.out.println("o =" +  o);
        boolean a = new Date(1970,4,28) == new Date(1970,4,28); // false because they have different references
        System.out.println(a);
        boolean b = (new Date(1970,4,28)).equals(new Date(1970,4,28)); // it should return true
        System.out.println(b);

        Date obj1 = new Date(1111,11,1);
        Date obj2 = new Date(1111,11,1);

        String S = "Hello";
        String S01 = new String("Hello");
        String S02 = new String("Hello");
        System.out.println(S == S02);
        System.out.println(obj1.equals(obj2));

        int i = 42;

        System.out.println(Math.sqrt(4));
        System.out.println(Math.E);
        System.out.println(System.currentTimeMillis());

        //Integer i01 = new Integer(4);
        Integer I01 = 5;
        Integer I02= 9;
        I01.toString();
        System.out.println(I01.compareTo(I02));
    }
}
