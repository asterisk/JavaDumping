class StaticBlock{
    static double rootOf2;
    static double rootOf3;

    static{
        System.out.println("inside static block");
        rootOf2 = Math.sqrt(2.0);
        rootOf3 = Math.sqrt(3.0);
    }
    StaticBlock(String msg){
        System.out.println(msg);
    }
}

class StaticErr{
    int denom=3;
    static int val=1024;
    static  int valDivDenom(){
        //return val/denom; // static methods can't use non-static variables - error compilation
        return 0;
    }
}
class Sum{
    int sum;
    Sum(int n){
        sum=0;
        for(int i=1; i<=n; i++)
            sum += i;
    }

    Sum(Sum obj){
        sum = obj.sum;
    }
}

class ConObj{
    int x;
    ConObj(){
        x = 0;
    }
    ConObj(int i){
        x = i;
    }
    ConObj(double d){
        x = (int) d;
    }
    ConObj(int a, int b){
        x = a*b;
    }
}
class ovlDempobj{
    void ovlDemp(){
        System.out.println("have sex");
    }
    void ovlDemp(int i){
        System.out.println("have sex with a number");
    }
    int ovlDemp(int a, int b){
        System.out.println("have sex with two numbers, also returning a number");
        return a + b;
    }
    double ovlDemp(double a,double b){
        System.out.println("have sex with doubles, returning a double");
        return a + b;
    }
}


class overloading{
    static public void main(String[] args){
        ovlDempobj od = new ovlDempobj();
        od.ovlDemp();
        od.ovlDemp(2);
        od.ovlDemp(2,3);
        od.ovlDemp(2.3,3.3);


        StaticBlock ob = new StaticBlock("inside cons");
        System.out.println("root of 2: " + StaticBlock.rootOf2);
        System.out.println("root of 3: " + StaticBlock.rootOf3);
    }
}
