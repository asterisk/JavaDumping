import java.io.*;

class cat{
    public static void main(String[] args) { 
        int i;
        FileInputStream fin = null; 
        if(args.length != 1) { // not like C2, this at least has to equal one (not two)
            System.out.println("Usage: cat filename");
            return;
        }

        try{ 
            fin = new FileInputStream(args[0]); 
        } catch(FileNotFoundException ex){
            System.out.println("File not found");
            return;
        }
        
        try { 
            do{
                i = fin.read();  
                if(i != -1) System.out.print((char) i);
            } while (i != -1);
        } catch(IOException ex ) {
            System.out.println("Error reading the file");
        }

        finally{
            try{ 
                if(fin != null) fin.close();
            } catch(IOException ex) {
                System.out.println("error prining the file");
            }
        }
    }
}
