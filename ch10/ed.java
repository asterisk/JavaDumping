import java.io.*;


class ed {
    public static void main(String[] args)  {
        String str;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter stop to quit");

        try(FileWriter fw = new FileWriter("edFile")) {
            do{
                str = br.readLine();
                if (str.compareTo("stop") == 0 ) break;
                str = str + "\r\n"; // new line?
                fw.write(str);
            } while (str.compareTo("stop") == 0 );
        } catch (IOException ex) {
            System.out.println("Error");
        }


    }
}
