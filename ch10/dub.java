import java.io.*;

class dub{
    public static void main(String[] args) throws IOException {
        int i;
        if(args.length != 2) { 
            System.out.println("Usage: dub InputFile OutputFile");
            return;
        }
        try(FileInputStream fin = new FileInputStream(args[0]); 
            FileOutputStream fout = new FileOutputStream(args[1])
                ){
            do { 
                i = fin.read();
                if(i != -1) fout.write(i);
            } while(i != -1);
        } catch( IOException ex ) {
            System.out.println("IO Error");
        }
    }
}
