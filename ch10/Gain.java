import java.io.*;

enum Transport{
    CAR(65), TRUCK(55), AIRPLANE(600), TRAIN(70), BOAT(22);

    private int speed;
    Transport(int s) { speed = s; }
    int getSpeed() { return speed;}
}

class Gain{
    public static void main(String[] args) {
        PrintWriter pw = new PrintWriter(System.out, true); // true for flashing on
        int i = 10;
        double d = 123.65;

        Transport tp, tp2, tp3;
        tp = Transport.AIRPLANE;
        tp2 = Transport.BOAT;
        tp3 = Transport.TRAIN;

        System.out.println(tp.compareTo(tp2));
        System.out.println(tp2.compareTo(tp));
        System.out.println(tp.compareTo(tp));

        pw.println("using a printwriter");
        pw.println(i);
        pw.println(d);
        pw.println(i + " + " + d + " is " + (i+d));
    }
}
