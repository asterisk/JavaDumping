public interface MotherFucker {
    public int getUserID();
    default int getAdminID() {
    return 1;
    }
}

class MotherFuckerClass implements MotherFucker {
    public int getUserID(){
        return 0;
    }
}

interface Const {
    int MIN = 0;
    int MAX = 10;
    String ERRORMSG = "Boundary Error";
}
public interface Series{
    int getNext();
    void reset();
    void setStart(int x);
}

class ByTwos implements Series {

    int start;
    int val;

    ByTwos() {
        start=0;
        val=0;
    }

    public int getNext() { // it has to be public
        val +=2;
        return val;
    }
    public void reset() {
        val = start;
    }
    public void setStart(int x) {
        start = x;
        val = x;
    }
}
