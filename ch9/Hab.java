import java.io.IOException;

class Hab {
    public static char prompt(String S) throws java.io.IOException {
        System.out.print(S + ": ");

        return(char) System.in.read();
    }
    public static void main(String[] args) {
        char ch;
        try{
            ch = prompt("Enter a letter");
        } catch(java.io.IOException ex) {
            System.out.println("IOEx occured");
            ch = 'X';
        }

        System.out.println("you pressed" + ch);
    }
}
