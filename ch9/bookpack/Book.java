package bookpack;

public class Book { 
    private String title;
    private String author;
    private int PubDate;

    public Book(String t, String a, int d) { 
        title=t;
        author=a;
        PubDate=d;
    }

    public void show(){ 
        System.out.println(title);
        System.out.println(author);
        System.out.print(PubDate);
        System.out.println();
    }

    public static void main(String[] args) { 
        Book books[] = new Book[2]; // to intialize an array
        books[0] = new Book("king kana", "ksjlfj", 4);
        books[1] = new Book("king lama", "ksjlfj", 4);
        for(Book b: books) b.show();
    }
}

